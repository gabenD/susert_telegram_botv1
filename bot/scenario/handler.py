from bot.scenario.handler_abstract import AbstractHandler, Request, Resp
from bot.database.db_json import json_db


class ScenarioHandlerText(AbstractHandler):
    """
    Обработчик по сценарию для Telegram (нажатия по клавиатуре)
    """
    page_work = 1

    @classmethod
    def data_from_step_by_id(cls, to_id, chat_id):
        print("Next strp show ->", to_id)
        page = json_db.get_page(cls.page_work)
        if page is None:
            return None
        step = page.get_by_id(to_id)
        if step is None:
            return None
        text = step.get("text")
        ans = []
        if text is not None:
            add = Resp.text_resp(chat_id, text, chooses=page.get_choose_texts_by_id(to_id))
            if add is not None:
                ans.append(add)

        more = step.get("more")
        if more is not None:
            # Image add response
            image = more.get("image")
            if image is not None:
                add = Resp.image_resp(chat_id, image)
                if add is not None:
                    ans.append(add)
            # Sticker add response
            sticker = more.get("sticker_id")
            if sticker is not None:
                add = Resp.sticker_resp(chat_id, sticker)
                if add is not None:
                    ans.append(add)
        return ans

    @classmethod
    def set_state(cls, state: int, request: Request):
        user_db = request.user
        user_db.status_id = int(state)
        user_db.save()

    @classmethod
    def start(cls, request: Request):
        start_id = 2
        cls.set_state(start_id, request)
        return cls.data_from_step_by_id(start_id, request.message.chat.id)

    def handle(self, request: Request):
        user_db = request.user
        message_text = request.message.text
        chat_id = request.message.chat.id
        load = json_db.get_page(self.page_work)
        user_status_id = user_db.status_id
        choose_to = load.get_choose_by_text_or_none(user_status_id, message_text)
        if choose_to is not None and choose_to.get("to_id") is not None:
            # Отлично, формируем ответ
            resp = self.data_from_step_by_id(choose_to["to_id"], chat_id)
            if resp is None:
                resp = self.data_from_step_by_id(user_status_id, chat_id)
                self.set_state(user_status_id, request)
            else:
                self.set_state(choose_to["to_id"], request)
            return resp
        else:
            # Default or response UP
            resp_up = super().handle(request)
            if resp_up is None:
                resp_up = self.data_from_step_by_id(user_status_id, chat_id)
                self.set_state(user_status_id, request)
            return resp_up


class StartHandler(AbstractHandler):
    """
    Обработчик по начальным настройкам
    """
    def handle(self, request: Request):
        user_db = request.user
        message_text = request.message.text
        chat_id = request.message.chat.id
        if user_db.status_id <= 0:
            chooses = ["Я готов 💎"]
            if message_text in chooses:
                return ScenarioHandlerText.start(request)
            text = "Предлагаю тебе немного пройтись по нашим достопримечательностям 👑"
            ans = [Resp.text_resp(chat_id, text, chooses=["Я готов 💎"])]
            return ans
        else:
            return super().handle(request)


def new_message(bot, *args, **kwargs):
    start_handler = StartHandler()
    scenario = ScenarioHandlerText()
    scenario.set_next(start_handler)
    responses = scenario.handle(Request(*args, **kwargs))
    if responses is not None:
        for i in range(len(responses) - 1, -1, -1):
            responses[i].action(bot)


