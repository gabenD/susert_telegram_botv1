from __future__ import annotations

import os
from abc import ABC, abstractmethod
from typing import Optional
from bot.scenario.buttons_creator import create_keyboard_by_texts
from bot.config import PATH_PHOTO


class Request(ABC):
    def __init__(self, user, message_telegram):
        self.user = user
        self.message = message_telegram


class Resp:
    def __init__(self, methods: str, args: list, kwargs: dict):
        self.methods = methods
        self.args = args
        self.kwargs = kwargs

    def action(self, bot):
        print(self.methods)
        print(self.args)
        print(self.kwargs)
        func = getattr(bot, self.methods, None)
        if func is not None:
            func(*self.args, **self.kwargs)

    @classmethod
    def text_resp(cls, chat_id, text, chooses=None, is_markdown=True):
        methods = "send_message"
        kwargs = dict(
            chat_id=chat_id,
            text=text,
        )
        if is_markdown:
            kwargs["parse_mode"] = "MarkdownV2"
        if chooses is not None and isinstance(chooses, list) and len(chooses) > 0:
            r = create_keyboard_by_texts(chooses)
            if r is not None:
                kwargs["reply_markup"] = r
        return cls(
            methods, [], kwargs
        )

    @classmethod
    def image_resp(cls, chat_id, path_image, caption=""):
        methods = "send_photo"
        try:
            doc = open(os.path.join(PATH_PHOTO, path_image), 'rb')
            return cls(
                methods, [chat_id, doc, caption], dict()
            )
        except:
            return None
        
    @classmethod
    def sticker_resp(cls, chat_id, sticker_id):
        pass


class Handler(ABC):
    """
    Интерфейс Обработчика объявляет метод построения цепочки обработчиков. Он
    также объявляет метод для выполнения запроса.
    """

    @abstractmethod
    def set_next(self, handler: Handler) -> Handler:
        pass

    @abstractmethod
    def handle(self, request: Request) -> Optional[str, Resp]:
        pass


class AbstractHandler(Handler):
    """
    Поведение цепочки по умолчанию может быть реализовано внутри базового класса
    обработчика.
    """

    _next_handler: Handler = None

    def set_next(self, handler: Handler) -> Handler:
        self._next_handler = handler
        # Возврат обработчика отсюда позволит связать обработчики простым
        # способом, вот так:
        # monkey.set_next(squirrel).set_next(dog)
        return handler

    @abstractmethod
    def handle(self, request: Request) -> Optional[str, Resp]:
        if self._next_handler:
            return self._next_handler.handle(request)

        return None
