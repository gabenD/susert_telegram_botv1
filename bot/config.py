import os

from peewee import (
    SqliteDatabase,
    MySQLDatabase,
)

TOKEN_TELEGRAM = os.getenv('TOKEN_TELEGRAM', '1194903090:AAGy9XJ-blVsWU9l5F2MKE-FrMRSEWoWAQs')
IS_PRODUCTION = os.getenv('IS_PRODUCTION', None)
API_LAYOUTS_URL = os.getenv('API_LAYOUTS_URL')
API_PROJECTS_URL = os.getenv('API_PROJECTS_URL')
API_ANALYTICS_URL = os.getenv('API_ANALYTICS_URL')
API_TOWNS_URL = os.getenv('API_TOWNS_URL')
API_AUTH_URL = os.getenv('API_AUTH_URL')

DB_NAME = os.getenv('DB_NAME', '')
DB_USER = os.getenv('DB_USER', '')
DB_HOST = os.getenv('DB_HOST', '')
DB_PORT = os.getenv('DB_PORT', '3306')
DB_PASSWORD = os.getenv('DB_PASSWORD', '')
PATH_PHOTO = os.getenv('PATH_PHOTO', '/home/gaben/PycharmProjects/susert_teleg/photos')
PATH_FILES_DB = os.getenv("PATH_FILES_DB", '/home/gaben/PycharmProjects/susert_teleg/bot/database/')


def is_production():
    return IS_PRODUCTION is not None


if is_production():
    database = MySQLDatabase(
        database=DB_NAME,
        user=DB_USER,
        host=DB_HOST,
        port=int(DB_PORT),
        password=DB_PASSWORD,
    )
else:
    database = SqliteDatabase('bot.db')

database.connect()
