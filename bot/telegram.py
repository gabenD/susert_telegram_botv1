import telebot
from telebot.types import KeyboardButton, ReplyKeyboardMarkup
import socket
import bot.database.models as models
from bot.config import *
from bot.scenario import handler
import time
bot = telebot.TeleBot(TOKEN_TELEGRAM)


@bot.message_handler(commands=['start'])
def start_message(message):
    start_key = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True).row(
        KeyboardButton("Погнали!")
    )
    bot.send_message(message.chat.id,
                     "👋 Привет, я Алексей Фёдорович Турчанинов - директор завода Сысертского завода. Я купил его в 1758 году. Мои рабочие выплавляют чугун, медь, а также готовые изделия.",
                     reply_markup=start_key)


@bot.message_handler(commands=['clear'])
def clear_utils_handler(message):
    if not is_production():
        user, is_create = models.User.get_or_create(telegram_id=message.chat.id)
        user.status_id = 0
        user.save()
        start_message(message)


@bot.message_handler(content_types=['text'])
def get_text_messages(message):
    user, is_create = models.User.get_or_create(telegram_id=message.chat.id)
    handler.new_message(
        bot=bot,
        user=user,
        message_telegram=message,
    )


def start_bot():
    while True:
        try:
            bot.polling(none_stop=True, interval=0)
        except Exception as err:
            print(err)
            time.sleep(5)
