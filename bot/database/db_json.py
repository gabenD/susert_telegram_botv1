import json
import os
from bot.config import PATH_FILES_DB


def open_json(file):
    with open(os.path.join(PATH_FILES_DB, file), "r") as fin:
        return json.loads(fin.read())


class Page:
    def __init__(self, file: str):
        self.data = open_json(file)
        print(self.data)

    def get_by_id(self, id):
        try:
            return self.data[str(id)]
        except:
            return None

    def get_choose_texts_by_id(self, uuid):
        a = self.get_by_id(str(uuid))
        if a is None:
            return a
        choose = a.get("choose")
        return [x.get("text") for x in choose if x.get("text") is not None]

    def get_choose_by_text_or_none(self, uuid, text):
        print("Text into get_choose_by_text_or_none\n", text)
        if text is None:
            return None
        a = self.get_by_id(str(uuid))
        print(a)
        if a is None or a.get("choose") is None or len(a.get("choose")) <= 0:
            return None
        for x in a["choose"]:
            if x.get("text") == text:
                return x
        return None


class JsonDB:
    def __init__(self):
        self.pages_object = open_json("pages.json")
        self.pages = dict()
        for page_item in self.pages_object:
            self.pages[int(page_item["id"])] = Page(str(page_item["id"]) + ".json")

    def get_page(self, id):
        try:
            return self.pages[id]
        except IndexError:
            return None


json_db = JsonDB()
