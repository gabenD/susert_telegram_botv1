import datetime

import peewee

from bot.config import (
    database,
)


class BaseModel(peewee.Model):
    class Meta:
        database = database


class User(BaseModel):
    telegram_id = peewee.IntegerField(unique=True, null=False, index=True)
    status_id = peewee.IntegerField(default=0, null=False)
    timestamp = peewee.DateTimeField(default=datetime.datetime.now)
