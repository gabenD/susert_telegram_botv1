import csv
import json

pages = dict()  # "id": {dict() -> info}
TEXT = "Text Area 1"


def to_func(func, string: str):
    try:
        return func(string)
    except ValueError:
        return None


def to_int(string):
    return to_func(int, string)


def to_float(string):
    return to_func(float, string)


def create_page(page_id, text):
    global pages
    if page_id not in pages:
        pages[page_id] = dict(
            name=text,
            data=dict()
        )
    else:
        pages[page_id]["name"] = text


def parse_page(item: dict):
    create_page(to_int(item["Id"]), item[TEXT])


def markdown_replace(markdown_text: str) -> str:
    dates = ("\"", "'", ":", ",", ";", ".", "!", "_", "?", "\n", "-", ")", "(", ">", "<", "[", "]")
    for date in dates:
        markdown_text = markdown_text.replace(date, "\\"+date)
    return markdown_text


def parse_process(item: dict):
    global pages
    page_id = to_int(item["Page ID"])
    item_id = to_int(item["Id"])
    text, more = get_more_and_text_by_string(item[TEXT])
    if page_id not in pages:
        create_page(page_id, "")
    choose_old = []
    if item_id in pages[page_id]["data"]:
        choose_old = pages[page_id]["data"][item_id]["choose"]
    text = markdown_replace(text)
    pages[page_id]["data"][item_id] = dict(
        id=item_id,
        text=text,
        more=more,
        choose=choose_old
    )


def get_more_and_text_by_string(text: str):
    """
    Парсинг основного текста и дополнительных полей из текстового поля процесса
    :param text: основной текст
    :return: text и +dict[str - ключ : "str - значение"]
    """
    def parse_add(more_text: str):
        array_value = list(more_text.split(":"))
        if len(array_value) < 2:
            return None, None
        key = array_value[0]
        value = str(":".join(array_value[1:])).strip().strip("\"")
        print("[", key, "]{", value, "}", sep="")
        return key, value


    def parse_one(item):
        parse_array = list(item.strip().split(" +"))
        print(parse_array)

        if len(parse_array) < 0:
            return "", dict()

        new_text = str(parse_array[0]).replace(" ", "\n")
        ans = dict()

        for i in range(1, len(parse_array)):
            key, value = parse_add(parse_array[i])
            if key is not None and value is not None:
                ans[key] = value
        return new_text, ans

    return parse_one(text.strip())


def parse_arrow(item: dict):
    global pages
    page_id = to_int(item["Page ID"])
    item_id = to_int(item["Id"])
    text = item[TEXT].strip()
    from_id = to_int(item["Line Source"])
    to_id = to_int(item["Line Destination"])
    if page_id not in pages:
        create_page(page_id, "")
    if from_id not in pages[page_id]["data"]:
        pages[page_id]["data"][from_id] = dict(choose=[])
    pages[page_id]["data"][from_id]["choose"].append(
        dict(
            id=item_id,
            from_id=from_id,
            to_id=to_id,
            text=text,
        )
    )


def export(many_files=True):
    global pages

    def to_json(a) -> bytes:
        return json.dumps(a, ensure_ascii=False).encode('utf8')

    if many_files:
        pages_array = []
        for key, value in pages.items():
            pages_array.append(dict(id=key, name=value["name"]))
            with open(str(key) + ".json", "wb") as f:
                f.write(to_json(value["data"]))
        with open("pages.json", "wb") as f:
            f.write(to_json(pages_array))
    else:
        with open("lines.json", "wb") as f:
            f.write(to_json(pages))


def parser_csv_to_json(file_obj):
    """
    Read a CSV file using csv.DictReader
    """

    reader = csv.DictReader(file_obj, delimiter=',')
    for row in reader:
        type_object = row["Name"]
        if type_object == "Page":
            parse_page(row)
        elif type_object == "Process" or type_object == "Custom Shape":
            parse_process(row)
        elif type_object == "Line":
            parse_arrow(row)


def parse(file="data.csv", many_files=True):
    with open(file) as f_obj:
        parser_csv_to_json(f_obj)
    export(many_files)


if __name__ == "__main__":
    parse()
